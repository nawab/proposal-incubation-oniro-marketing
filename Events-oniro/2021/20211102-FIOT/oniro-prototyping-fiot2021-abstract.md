# prototyping on oniro @ FIOT2021 #

URL: https://future-iot.org/2021/12/02/4th-future-iot-philippe-coval-keynote-prototyping-on-oniro/#2021


## Abstract: ##

Based on his experiences, Philippe will explain the gap between a maker prototype
and a sustainable ecosystem that would match market's standards.

Publishing a code under "FLOSS" license is a good start but it's usually not enough
for project sustainability, like grabbing some component for "free" will have a cost soon or later.

The opensource philosophy is to not reinvent the wheel,
so your value will probably work on common base, but choose wisely !
What does this platform provide in term of terms of security, IP compliance ?
What is the cost of hardware enablement or just software maintenance ?

Talk will be illustrated with some demos from previous Automotive "CampOSV" sessions in Rennes,
the Oniro project will be also introduced and how prototyping can be essential
for the development of foundations of a bigger ambitious ecosystem.

Oniro project is a decentralized Operating system which aims to defragment software development
by using the best components and practices already adopted by community and industry
(like Linux or Zephyr kernel and Yocto's bitbake tool).

Starting by doing is a good way to learn,
working alone can be quick but working together will bring you further,

Everyone is welcome now to join the oniro project at https://oniroproject.org/

## Bio: ##

Philippe Coval is a professional OpenSource Engineer,
who has been supporting FLOSS communities since late 1990ies.
Over decades, he has contributed to many projects from Operating Systems (Debian, Tizen)
to IoT frameworks (IoTivity, WebThings) and more.

He also advocated for FLOSS at various occasions (FOSDEM, ELC), or shared proof of concepts online.
Currently contributing to the Oniro project he is always open for potential cooperation.
